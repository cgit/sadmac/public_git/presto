#!/usr/bin/python -t
# -*- mode: Python; indent-tabs-mode: nil; -*-
#
# Slightly modified from the Fedora Infrastructure RepoPrune.py script
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import errno, os, sys
import fnmatch, re
import rpmUtils.transaction, rpmUtils.miscutils
import string

KEEP = 1
DEBUG = False

def getFileList(path, ext, filelist=[]):
    """Return all files in path matching ext, store them in filelist,
    recurse dirs. Returns a list object"""

    extlen = len(ext)
    totalpath = os.path.normpath(path)
    try:
        dir_list = os.listdir(totalpath)
    except OSError, e:
        errorprint(_('Error accessing directory %s, %s') % (totalpath, e))
        sys.exit(1)

    for d in dir_list:
        if os.path.isdir(totalpath + '/' + d):
            filelist = getFileList(os.path.join(totalpath, d), ext, filelist)
        else:
            if string.lower(d[-extlen:]) == '%s' % (ext):
                filelist.append(os.path.join(totalpath, d))

    return filelist

def pruneRepo(keep,srcdir):
    ts = rpmUtils.transaction.initReadOnlyTransaction()
    changed = False
    
    # Create list of src.rpm files.
    # We don't use "glob", so sub-directories are supported.
    print 'Expiring (keep=%d):' % keep, srcdir

    rpmfiles = getFileList(srcdir, ".rpm")
    newestrpms = {}
    for f in rpmfiles:
        try:
           hdr = rpmUtils.miscutils.hdrFromPackage(ts,f)
        except:
           print "Unable to open %s" % f
        else:
            n = hdr['name']
            v = hdr['version']
            r = hdr['release']
            e = hdr['epoch']
            a = hdr['arch']
            if e is None:
                e = 0
            newestrpms.setdefault("%s.%s" % (n,a),[])
            newestrpms["%s.%s" % (n,a)].append((f,n,e,v,r,a))

    # Now purge old src.rpm unless their %name matches a white-list pattern.
    for l in newestrpms.values():
        x = len(l)

        if x > 1:
            def sortByEVR(fnevr1, fnevr2):
                (f1,n1,e1,v1,r1,a1) = fnevr1
                (f2,n2,e2,v2,r2,a2) = fnevr2
                rc = rpmUtils.miscutils.compareEVR((e1,v1,r1),(e2,v2,r2))
                if rc == 0:
                    return 0
                if rc > 0:
                    return -1
                if rc < 0:
                    return 1
            
            l.sort(sortByEVR)  # highest first in list
        
        oldies = []
        if len(l) > abs(keep):
            oldies = l[keep:]
        for (f,n,e,v,r,a) in oldies:
            print '  Removing', os.path.basename(f)
            rpmfiles.remove(f)
            if not DEBUG:
                os.remove(f)


def main(srcdir):
    assert rpmUtils.miscutils.compareEVR((1,2,3),(1,2,0)) > 0
    assert rpmUtils.miscutils.compareEVR((0,1,2),(0,1,2)) == 0
    assert rpmUtils.miscutils.compareEVR((1,2,3),(4,0,99)) < 0

    keep = KEEP
        
    return pruneRepo(keep, srcdir)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'Usage: %s <folder>\n' % os.path.basename(sys.argv[0])
        sys.exit(errno.EINVAL)

    main(sys.argv[1])
    sys.exit(0)
