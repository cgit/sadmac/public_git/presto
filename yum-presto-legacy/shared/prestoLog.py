# author: Jonathan Dieter <jdieter@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# Copyright 2005 Duke University

class PrestoLog:
    def __init__(self, conduit, log_filename):
        # Open log file for reading
        try:
            log_file = open(log_filename, "r")
            log_exists = True
        except:
            conduit.info(7, "Info: %s doesn't exist. Will create." % log_filename)
            log_exists = False
            
        # Log file doesn't exist, create
        if not log_exists:
            try:
                log_file = open(log_filename, "w")
                log_file.close()
                log_exists = True
            except:
                conduit.info(2, "Warning: Unable to write to %s" % log_filename)
            if log_exists:
                try:
                    log_file = open(log_filename, "r")
                except:
                    conduit.info(2, "Warning: Unable to open %s for reading." % log_filename)
                    log_exists = False
                    
        # Cycle through items already in log so we can come up with total savings
        if log_exists:
            self.total_rpm_size = 0
            self.total_drpm_size = 0
            
            # Get rid of header line
            log_file.readline()
            
            data = log_file.readline()
            while data != "":
                fc = data.rfind("-")
                sc = data.rfind("-", 0, fc-1)
                tc = data.rfind("-", 0, sc-1)
                lc = data.rfind("-", 0, tc-1)
                if lc != -1 and tc != -1 and sc != -1 and fc != -1:
                    self.total_rpm_size += int(data[lc+1:tc])
                    self.total_drpm_size += int(data[tc+1:sc])
                data = log_file.readline()
            log_file.close()
            
            try:
                log_file = open(log_filename, "a")
            except:
                conduit.info(2, "Warning: Unable to open %s for writing." % log_filename)
                self.log_file = None
            else:
                self.log_file = log_filename
                log_file.close()

    def log(self, oldrpm_name, newrpm_name, rpm_size, drpm_size):
            # Write data to log
            self.total_rpm_size  += rpm_size
            self.total_drpm_size += drpm_size
            if self.log_file != None:
                try:
                    log_file = open(self.log_file, "a")
                except:
                    pass
                else:
                    log_file.write("%s => %s - %i - %i - %i - %i\n" % (oldrpm_name, newrpm_name, rpm_size, drpm_size, 100 - ((drpm_size * 100) / rpm_size), 100 - ((self.total_drpm_size * 100) / self.total_rpm_size)))
                    log_file.close()

                
    def close(self):
        if self.log_file != None:
            try:
                self.log_file.close()
            except:
                pass
        
