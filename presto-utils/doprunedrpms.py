#!/usr/bin/python -t
# -*- mode: Python; indent-tabs-mode: nil; -*-
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import errno, os, sys
import fnmatch, re
import rpmUtils.transaction, rpmUtils.miscutils
import string

DEBUG = False

def getFileList(path, ext, filelist=[]):
    """Return all files in path matching ext, store them in filelist,
    recurse dirs. Returns a list object"""

    extlen = len(ext)
    totalpath = os.path.normpath(path)
    try:
        dir_list = os.listdir(totalpath)
    except OSError, e:
        errorprint(_('Error accessing directory %s, %s') % (totalpath, e))
        sys.exit(1)

    for d in dir_list:
        if os.path.isdir(totalpath + '/' + d):
            filelist = getFileList(os.path.join(totalpath, d), ext, filelist)
        else:
            if string.lower(d[-extlen:]) == '%s' % (ext):
                filelist.append(os.path.join(totalpath, d))

    return filelist

def pruneRepo(srcdir):
    ts = rpmUtils.transaction.initReadOnlyTransaction()
    changed = False
    
    # Create list of src.rpm files.
    # We don't use "glob", so sub-directories are supported.
    print 'Expiring :', srcdir

    rpmfiles = getFileList(srcdir, ".rpm")
    foundrpms = {}
    for f in rpmfiles:
        try:
           hdr = rpmUtils.miscutils.hdrFromPackage(ts,f)
        except:
           print "Unable to open %s" % f
        else:
            n = hdr['name']
            v = hdr['version']
            r = hdr['release']
            e = hdr['epoch']
            a = hdr['arch']
            if e is None:
                e = 0
            foundrpms.setdefault((n,e,v,r,a), True)

    drpmfiles = getFileList(srcdir, ".drpm")

    rm_files = []
    for f in drpmfiles:
        try:
           hdr = rpmUtils.miscutils.hdrFromPackage(ts,f)
        except:
           print "Unable to open %s" % f
        else:
            n = hdr['name']
            v = hdr['version']
            r = hdr['release']
            e = hdr['epoch']
            a = hdr['arch']
            if e is None:
                e = 0
            if not foundrpms.has_key((n,e,v,r,a)):
                rm_files.append(f)

    for f in rm_files:
        print '  Removing', os.path.basename(f)
        if not DEBUG:
            os.remove(f)


def main(srcdir):
    assert rpmUtils.miscutils.compareEVR((1,2,3),(1,2,0)) > 0
    assert rpmUtils.miscutils.compareEVR((0,1,2),(0,1,2)) == 0
    assert rpmUtils.miscutils.compareEVR((1,2,3),(4,0,99)) < 0

    return pruneRepo(srcdir)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'Usage: %s <folder>\n' % os.path.basename(sys.argv[0])
        sys.exit(errno.EINVAL)

    main(sys.argv[1])
    sys.exit(0)
